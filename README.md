# Authelia + LDAP Auth + Web backend

### Test user credentials
```
Username: test
Password: c3191ef0-c2e5-4751-8714-e8dacef0067c
```

### Pre-requisites
Edit docker-compose.yml and authelia/configuration.yml with your chosen root domain, to pass LetsEncrypt validations
DNS will need to resolve for the sub-domains of  {public,secure,authelia}.$ROOTDOMAIN 

### Initialize OpenLDAP with test user
```
docker compose up openldap -d
./add-user.sh 
```


### Initialize backend and Authelia services
```
docker compose up -d
```

### Navigate to public.$ROOTDOMAIN to auth with test user credentials
